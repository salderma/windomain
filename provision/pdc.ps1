# Create PDC

Install-WindowsFeature AD-Domain-Services -IncludeManagementTools

$SecureSafePasswd = ConvertTo-SecureString "3edc4rfv#EDC$RFV" -AsPlainText -Force

Install-ADDSForest -DomainName "test.local" -DomainNetbiosName "TEST" -InstallDns:$true -SafeModeAdministratorPassword $SecureSafePasswd -Force

# -NoRebootOnCompletion:$true

