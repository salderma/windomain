# Add dc02 as Domain Controller

Install-WindowsFeature AD-Domain-Services -IncludeManagementTools

$SecureSafePasswd = ConvertTo-SecureString "3edc4rfv#EDC$RFV" -AsPlainText -Force
$Cred = Get-Credential "TEST\Administrator"

Install-ADDSDomainController -DomainName "test.local" -InstallDns -Credential $Cred -Force -SafeModeAdministratorPassword $SecureSafePasswd
